FROM ubuntu:18.04

MAINTAINER Dmitry Alekseev <i.mark.vortman@gmail.com>

# Install necessary utilits

RUN apt-get update \
    && apt-get -y --no-install-recommends install wget ca-certificates

# Download and install zabbix package

RUN wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+bionic_all.deb \
	&& dpkg -i zabbix-release_5.0-1+bionic_all.deb


RUN apt update \
	&& apt install -y zabbix-server-mysql \
	&& mkdir -p /var/run/zabbix \
	&& chown zabbix:zabbix /var/run/zabbix

# Port for communication with agents

EXPOSE 10051/TCP

# Run as zabbix user

USER 102

# Run service in foreground mode

CMD ["/usr/sbin/zabbix_server", "--foreground", "-c", "/etc/zabbix/zabbix_server.conf"]